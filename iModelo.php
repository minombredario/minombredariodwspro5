<?php

    interface iModelo{
        public function createAsignatura($asignatura);
        public function readAsignatura();
        public function udpdateAsignatura($asignatura);
        public function deleteAsignatura($data);
        
        public function createProfesor($profesor);
        public function readProfesor();
        public function updateProfesor($profesor);
        public function deleteProfesor($data);
        
        public function instalarBD();
        public function desinstalar();
        
    }

?>

