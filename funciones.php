<?php

include_once ('modeloFichero.php');


function recoge($valor) {
    $resultado = "";
    
    if (isset($_REQUEST[$valor])) {
        $resultado = htmlspecialchars(trim(strip_tags($_REQUEST[$valor])), ENT_QUOTES, "UTF-8");
    }
    
    return $resultado;
}

function getId($fichero) {
    
    $ultId="0";
    $registros= array();
    
    if(is_file("ficheros/".$fichero.".csv")){
        $modelo = new ModeloFichero();
        if($fichero=="profesor"){
            $registros = $modelo->readProfesor();//guardo todos los registros del .csv en un array
        }
        if($fichero=="asignatura"){
            
            $registros = $modelo->readAsignatura();//guardo todos los registros del .csv en un array
        }
        $ultregistro = end($registros);//con end() averiguo cual es el ultimo elemento del array
        $ultId = $ultregistro->getId();
        $ultId++;
        return $ultId; 
    }else{
        $ultId = "1";
        return $ultId; 
        
    }
    
      
 
}
