<?php 
            
            require_once ("header.php");
            require ("Config.php");
        ?>
        
        
          <table width="50%" border="0" align="center">
               
                <thead>
                    <tr>
                        <th>
                             <h2><?php echo Config::$tema ?></h2>
                        </th>
                    </tr>
                       
                </thead>
                             
                <tr>
                    <td class="primera_fila inicio">
                        Elegir:
                    </td>
                </tr> 
            
                <tr>
                    <td style="text-align: align;">
                        <input class="boton" type="button" onclick="location.href = 'VistaProfesores.php'" name= "profesor" value="Gestión Profesor"/>
                        <input class="boton" type="button" onclick="location.href = 'VistaAsignaturas.php'" name="asignatura" value="Gestión Asignatura"/>
                        <input class="boton" type="button" onclick="location.href = 'VistaInstalacion.php'" name="instalar" value="Instalar BBDD"/>
                    </td>
                </tr>
            
                <tr >
                    <td class="primera_fila inicio">
                        Documentación por tema:
                    </td>
                </tr> 
                
                <tr>
                    <td class="resto_filas">
                        <?php 
                            foreach(Config::$Documentacion as $doc){ 
                                echo "<li><a href='Documentacion/".$doc.".pdf'  target='_blank'>". $doc ." Enunciado</a></li>";
                            }
                        ?>
                    </td>
                </tr>
                
                <tr>
                    <td class="primera_fila inicio">
                        Documentación de este proyecto:
                    </td>
                </tr>
                
                <tr>
                    <td class="resto_filas">
                        <a href="Documentacion/Tema5_documentacion.pdf" target="_blank">Documentación</a>
                    </td>
                </tr>
          </table>

            <?php 
            
            require_once ("footer.php"); 
        ?>